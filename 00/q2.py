#!/usr/bin/env python3

class Rectangle(object):
    """ Implements a simple rectangle class '"""
    def __init__(self, width=1, height=2):
        """ initialise rectangle object """
        self.width = width
        self.height = height

    def __str__(self):
        """ prints rectangle with # char """
        template = ""
        for _ in range(self.height):
            template += "#" * self.width + '\n'

        return template[:-1]
