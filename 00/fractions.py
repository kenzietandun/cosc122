#!/usr/bin/env python3

class Fraction(object):
    """ fraction """

    def __init__(self, num=0, denom=1):
        """ creates a new fraction """
        self.numerator = num
        if denom != 0:
            self.denominator = denom
        else:
            raise ZeroDivisionError

    def __repr__(self):
        """ representation of Fraction """
        return "Fraction({}, {})""".format(
                self.numerator,
                self.denominator
                )

    def __str__(self):
        """ prints in fraction notation """
        return "{}/{}".format(
                self.numerator,
                self.denominator
                )

    def __add__(self, other):
        """ adds 2 fractions """
        numerator = self.numerator * other.denominator + \
                    self.denominator * other.numerator

        denominator = self.denominator * other.denominator
        return Fraction(numerator, denominator)

    def __mul__(self, other):
        """ multiplies 2 fractions """
        numerator = self.numerator * other.numerator
        denominator = self.denominator * other.denominator 
        return Fraction(numerator, denominator)

    def __eq__(self, other):
        """ checks if 2 fractions are equal """
        return self.numerator * other.denominator == \
               self.denominator * other.numerator

def findgcd(numerator, denominator):
    '''Returns the Greatest Common Divisor of x and y. Assumes x and y are positive integers.'''
    smaller = min(numerator, denominator)
    for i in range(smaller, 1, -1):
        if numerator % i == 0 and denominator % i == 0:
            return i
    return 1

class ReducedFraction(Fraction):
    """ reduced fraction """

    def __init__(self, numerator, denominator=1):
        """ initialises reduced fraction object """
        super().__init__(numerator, denominator)
        self.__reduce__()

    def __reduce__(self):
        """ reduces fraction to smallest """
        common_denominator = findgcd(self.numerator, self.denominator)
        self.numerator = int(self.numerator / common_denominator)
        self.denominator = int(self.denominator / common_denominator)

    def __repr__(self):
        """ representation of ReducedFraction """
        return "ReducedFraction({}, {})""".format(
                self.numerator,
                self.denominator
                )

    def __add__(self, other):
        """ adds 2 ReducedFractions """
        new_fraction = super().__add__(other)
        numerator = new_fraction.numerator
        denominator = new_fraction.denominator
        return ReducedFraction(numerator, denominator)

    def __mul__(self, other):
        """ multiplies 2 ReducedFractions """
        new_fraction = super().__mul__(other)
        numerator = new_fraction.numerator
        denominator = new_fraction.denominator
        return ReducedFraction(numerator, denominator)

class MixedNumber(object):
    """ mixed number """

    def __init__(self, number, fraction):
        """ initialise MixedNumber object """
        self.number = number
        numerator = fraction.numerator
        denominator = fraction.denominator
        self.reduced_fraction = ReducedFraction(
                numerator,
                denominator
                )

    def __repr__(self):
        """ representation of MixedNumber object """
        return "MixedNumber({}, {})".format(
                self.number, 
                self.reduced_fraction.__repr__()
                )

    def __str__(self):
        """ representation of MixedNumber object """
        return "{} and {}".format(
                self.number, 
                self.reduced_fraction
                )

    def __add__(self, other):
        """ adds 2 MixedNumber object """
        new_fraction = self.reduced_fraction + \
                       other.reduced_fraction

        quotient = 0
        if new_fraction.numerator >= new_fraction.denominator:
            quotient = new_fraction.numerator // new_fraction.denominator
            new_fraction.numerator = new_fraction.numerator - \
                    quotient * new_fraction.denominator

        new_number = self.number + quotient + other.number

        return MixedNumber(new_number, new_fraction)
