#!/usr/bin/env python3

class Rectangle(object):
    """ Rectangle class """

    def __init__(self, width=1, height=2):
        """ initialise rectangle with width and height """
        self.width = width
        self.height = height

    def area(self):
        """ returns area of rectangle """
        return self.width * self.height

    def perimeter(self):
        """ returns perimeter of rectangle """
        return 2 * (self.width + self.height)
