#!/usr/bin/env python3

#---------------------------
# Written by Kenzie Tandun
# Jul 29, 2018
# nudnateiznek@gmail.com
#---------------------------

class Queue():
    """queue object definition

    >>> from q2 import Queue
    >>> q = Queue()
    >>> q.enqueue('a')
    >>> print('Dequeued', q.dequeue())
    Dequeued a
    >>> q.enqueue('a')
    >>> q.enqueue('b')
    >>> print('Length is', len(q))
    Length is 2
    >>> print('Dequeued', q.dequeue())
    Dequeued a
    >>> print('Length is', len(q))
    Length is 1
    >>> print('Dequeued', q.dequeue())
    Dequeued b
    >>> try:
    ...     q.dequeue()
    ... except IndexError as error:
    ...     print(error)
    ...
    Can't dequeue from an empty queue!


    """

    def __init__(self):
        """initialise queue object"""
        self.items = []

    def enqueue(self, item):
        """adds item to queue"""
        self.items.append(item)

    def dequeue(self):
        """dequeues item from the queue
        returns the item in front of the list"""
        if not self.items:
            raise IndexError("Can't dequeue from an empty queue!")

        first_item = self.items.pop(0)
        return first_item

    def __len__(self):
        """returns the length of the queue"""
        return len(self.items)

def main():
    import doctest
    doctest.testmod()

if __name__ == "__main__":
    main()
