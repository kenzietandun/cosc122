import sys
import random
import time
from q1 import Stack
from q2 import Queue
from deque import Deque


# Get the time units given by the perf_counter
# Note: time.clock has been deprecated in Python 3.3
# and replaced with the more precise perf_counter method
# define get_time to be the appropriate time counter
if sys.version_info < (3, 3):
    get_time = time.clock
    print("Using time.clock for timing - Python ver < 3.3")
else:
    get_time = time.perf_counter
    print("Using time.perf_counter for timing - Python ver >= 3.3")
    REZ = time.get_clock_info('perf_counter').resolution
    print('Smallest unit of time is ' + str(REZ) + ' seconds')



def time_stack_push(initial_size, n_trials):
    """ Finds average time for pushing onto a stack """
    s = Stack()
    # s._items = [0] * initial_size  # is cunning but sometimes causes weird timings
    # so simply push lots of items onto the stack
    # pre_fill stack with initial_size zeros (feel free to push other things)
    for _ in range(initial_size):
        s.push(random.randint(0,127))

    # time some pushes
    start_time = get_time()
    for i in range(n_trials):
        s.push(0)
    end_time=get_time()
    time_per_operation=(end_time - start_time)/n_trials
    fmt = "Initial Stack size = {:,d} -> avg. time/push for {:,d} pushes is {:10.8f}"
    print((fmt.format(initial_size, n_trials, time_per_operation)))




# Do some creative copy and pasting here to make functions for other time trials
# ---start student section---
def time_queue_dequeue(initial_size, n_trials):
    """ Finds average time for pushing onto a stack """
    s = Queue()
    # s._items = [0] * initial_size  # is cunning but sometimes causes weird timings
    # so simply push lots of items onto the stack
    # pre_fill stack with initial_size zeros (feel free to push other things)
    for _ in range(initial_size):
        s.enqueue(random.randint(0,127))

    # time some pushes
    for i in range(n_trials):
        s.enqueue(0)

    start_time = get_time()
    for i in range(n_trials):
        s.dequeue()
    end_time=get_time()
    time_per_operation=(end_time - start_time)/n_trials
    fmt = "Initial Queue size = {:,d} -> avg. time/dequeue for {:,d} dequeues is {:10.8f}"
    print((fmt.format(initial_size, n_trials, time_per_operation)))

def time_stack_pop(initial_size, n_trials):
    """ Finds average time for pushing onto a stack """
    s = Stack()
    # s._items = [0] * initial_size  # is cunning but sometimes causes weird timings
    # so simply push lots of items onto the stack
    # pre_fill stack with initial_size zeros (feel free to push other things)
    for _ in range(initial_size):
        s.push(random.randint(0,127))

    # time some pushes
    for i in range(n_trials):
        s.push(0)
    start_time = get_time()
    for i in range(n_trials):
        s.pop()
    end_time=get_time()
    time_per_operation=(end_time - start_time)/n_trials
    fmt = "Initial Stack size = {:,d} -> avg. time/pop for {:,d} pops is {:10.8f}"
    print((fmt.format(initial_size, n_trials, time_per_operation)))

def time_queue_enqueue(initial_size, n_trials):
    """ Finds average time for pushing onto a stack """
    s = Queue()
    # s._items = [0] * initial_size  # is cunning but sometimes causes weird timings
    # so simply push lots of items onto the stack
    # pre_fill stack with initial_size zeros (feel free to push other things)
    for _ in range(initial_size):
        s.enqueue(random.randint(0,127))

    # time some pushes
    start_time = get_time()
    for i in range(n_trials):
        s.enqueue(0)
    end_time=get_time()
    time_per_operation=(end_time - start_time)/n_trials
    fmt = "Initial Queue size = {:,d} -> avg. time/push for {:,d} pushes is {:10.8f}"
    print((fmt.format(initial_size, n_trials, time_per_operation)))

def time_deque_enqueue_front(initial_size, n_trials):
    """ Finds average time for pushing onto a stack """
    s = Deque()
    # s._items = [0] * initial_size  # is cunning but sometimes causes weird timings
    # so simply push lots of items onto the stack
    # pre_fill stack with initial_size zeros (feel free to push other things)
    for _ in range(initial_size):
        s.enqueue_front(random.randint(0,127))

    # time some pushes
    start_time = get_time()
    for i in range(n_trials):
        s.enqueue_front(0)
    end_time=get_time()
    time_per_operation=(end_time - start_time)/n_trials
    fmt = "Initial Deque size = {:,d} -> avg. time/enq_front for {:,d} enqueues is {:10.8f}"
    print((fmt.format(initial_size, n_trials, time_per_operation)))

def time_deque_enqueue_rear(initial_size, n_trials):
    """ Finds average time for pushing onto a stack """
    s = Deque()
    # s._items = [0] * initial_size  # is cunning but sometimes causes weird timings
    # so simply push lots of items onto the stack
    # pre_fill stack with initial_size zeros (feel free to push other things)
    for _ in range(initial_size):
        s.enqueue_rear(random.randint(0,127))

    # time some pushes
    start_time = get_time()
    for i in range(n_trials):
        s.enqueue_rear(0)
    end_time=get_time()
    time_per_operation=(end_time - start_time)/n_trials
    fmt = "Initial Deque size = {:,d} -> avg. time/enq_rear for {:,d} enqueues is {:10.8f}"
    print((fmt.format(initial_size, n_trials, time_per_operation)))

def time_deque_dequeue_front(initial_size, n_trials):
    """ Finds average time for pushing onto a stack """
    s = Deque()
    # s._items = [0] * initial_size  # is cunning but sometimes causes weird timings
    # so simply push lots of items onto the stack
    # pre_fill stack with initial_size zeros (feel free to push other things)
    for _ in range(initial_size):
        s.enqueue_front(random.randint(0,127))

    # time some pushes
    for i in range(n_trials):
        s.enqueue_front(0)
    start_time = get_time()
    for i in range(n_trials):
        s.dequeue_front()
    end_time=get_time()
    time_per_operation=(end_time - start_time)/n_trials
    fmt = "Initial Deque size = {:,d} -> avg. time/dequeue_front for {:,d} dequeues is {:10.8f}"
    print((fmt.format(initial_size, n_trials, time_per_operation)))

def time_deque_dequeue_rear(initial_size, n_trials):
    """ Finds average time for pushing onto a stack """
    s = Deque()
    # s._items = [0] * initial_size  # is cunning but sometimes causes weird timings
    # so simply push lots of items onto the stack
    # pre_fill stack with initial_size zeros (feel free to push other things)
    for _ in range(initial_size):
        s.enqueue_rear(random.randint(0,127))

    # time some pushes
    for i in range(n_trials):
        s.enqueue_rear(0)
    start_time = get_time()
    for i in range(n_trials):
        s.dequeue_rear()
    end_time=get_time()
    time_per_operation=(end_time - start_time)/n_trials
    fmt = "Initial Deque size = {:,d} -> avg. time/dequeue_rear for {:,d} dequeues is {:10.8f}"
    print((fmt.format(initial_size, n_trials, time_per_operation)))

# ===end student section===



def run_tests():
    """ Runs as many or as few tests as you call,
    initially just runs the test for stack pushes
    """
    print('\n'*3)
    initial_size = 10000  # start with this many items in data structure
    n_trials = 100  # run this many trials and take the average time

    #time_stack_push(initial_size, n_trials)
    #time_stack_pop(initial_size, n_trials)
    #time_queue_enqueue(initial_size, n_trials)
    #time_queue_dequeue(initial_size, n_trials)

    time_deque_enqueue_front(initial_size, n_trials)
    time_deque_enqueue_rear(initial_size, n_trials)
    time_deque_dequeue_front(initial_size, n_trials)
    time_deque_dequeue_rear(initial_size, n_trials)
    # call your shiny new test functions here



run_tests()
