from q1 import Stack
from q2 import Queue
from deque import Deque
import re
import doctest
import os

OP_PREC={"(":1,
         "+":2,
         "-":2,
         "*":3,
         "/":3,
         ")":4}



def calculate(operator, param1, param2):
    """
    Returns the result of a calculation between param1 and param2 using the
    given operator.
    Note: results may be floats...
    Supported operators: *, /, +, -, ^
    NOTE: We will use ^ to indicate ** for this exercise

    >>> calculate('*', 2, 3)
    6
    >>> calculate('+', 2, 3)
    5
    >>> calculate('-', 2, 3)
    -1
    """
    if operator == '*':
        return param1 * param2
    elif operator == '/':
        return param1 / param2
    elif operator == '+':
        return param1 + param2
    elif operator == '-':
        return param1 - param2
    else:
        raise Exception(operator+" is an invalid operator!")



def evaluate_postfix(expression):
    """
    Evaluates an expression in postfix notaion.
    Operands in the given expression must be integers.

    IMPORTANT NOTES:
    Make sure you operate on operands in the right order
    For example:  3 2 -   should be 3 - 2, not 2 - 3

    Intermediate values may be floats (eg, 3/4 gives 0.75)
    so don't cast anything to int after the input phase.

    >>> evaluate_postfix('2 3 +')
    5.0
    >>> evaluate_postfix('2 3 4 * +')
    14.0
    >>> evaluate_postfix('2 3 + 4 *')
    20.0
    >>> evaluate_postfix('2 3 2 * + 5 -')
    3.0
    >>> evaluate_postfix('2 3 + 2 5 - *')
    -15.0
    >>> evaluate_postfix('2 3 + 5 2 / *')
    12.5
    >>> evaluate_postfix('2 3 4 8 + * + 1 + 4 * 5 -')
    151.0
    """
    # Split postfix string into tokens. For example:
    #  '2 3 +' => ['2', '3', '+']
    # Don't worry about how this works
    tokens = re.findall(r'(\d+|\*|\+|\/|\-|\)|\(|\^)', expression)

    s = Stack()
    final_result = 0
    for token in tokens:
        if token.isdigit():
            s.push(float(token))
        else:
            operator = token
            snd_operand = s.pop()
            fst_operand = s.pop()
            result = calculate(token, fst_operand, snd_operand)
            s.push(result)

    final_result = s.pop()
    return final_result

def infix_to_postfix(infix_expression):
    """
    Converts an infix expression to a postfix expression.
    Operands in the given expression must be integers.

    >>> infix_to_postfix('2 + 3')
    '2 3 +'
    >>> infix_to_postfix('2 + 3 * 4')
    '2 3 4 * +'
    >>> infix_to_postfix('(2 + 3) * 4')
    '2 3 + 4 *'
    >>> infix_to_postfix('2 + 3 * 2 - 5')
    '2 3 2 * + 5 -'
    >>> infix_to_postfix('(2 + 3) * (2 - 5)')
    '2 3 + 2 5 - *'
    >>> infix_to_postfix('(2 + 3) * (5 / 2)')
    '2 3 + 5 2 / *'
    >>> infix_to_postfix('2 + 3 * 4 / (6 - 4) + 1')
    '2 3 4 * 6 4 - / + 1 +'
    """

    # Split infix string into tokens. For example:
    #  '2+3*4' => ['2', '+', '3', '*', '4']
    # Operands must be integers.
    # Don't worry too much about how...
    tokens = re.findall(r'(\d+|\*|\+|\/|\-|\)|\(|\^)', infix_expression)

    output_list = []
    opstack = Stack()

    for token in tokens:
        if token.isdigit():
            output_list.append(token)
        elif token == '(':
            opstack.push(token)
        elif token == ')':
            while opstack.peek() != '(':
                output_list.append(opstack.pop())
            opstack.pop()
        else:
            while len(opstack) > 0:
                token_prec = OP_PREC[token]
                stack_top_prec = OP_PREC[opstack.peek()]
                if stack_top_prec >= token_prec:
                    stack_top = opstack.pop()
                    output_list.append(stack_top)
                else:
                    break
            opstack.push(token)

    while len(opstack) > 0:
        item = opstack.pop()
        if item in OP_PREC:
            output_list.append(item)

    return ' '.join(output_list)

def evaluate_infix(infix_expression):
    """
    Evaluates an infix expression.
    Operands in the given expression must be integers.

    >>> evaluate_infix('2 + 3 * 4')
    14.0
    >>> evaluate_infix('2 + (3 * 4)')
    14.0
    >>> evaluate_infix('(2 + 3) * 4')
    20.0
    >>> evaluate_infix('2 + 3 * 2 - 5')
    3.0
    >>> evaluate_infix('(2 + 3) * (2 - 5)')
    -15.0
    >>> evaluate_infix('(2 + 3) * (5 / 2)')
    12.5
    """

    # Split infix string into tokens. For example:
    #  '2+3*4' => ['2', '+', '3', '*', '4']
    # Operands must be integers
    # Don't worry too much about how...
    tokens = re.findall(r'(\d+|\*|\+|\/|\-|\)|\(|\^)', infix_expression)

    # Code to process tokens and evaluate the infix expression
    # See the Extras section of the handout.
    # As an extra exercise students can write code here that
    # evaluates infix directly (ie without converting to postfix first).
    # ---start student section---
    pass
    # ===end student section===




def evaluate_prefix(prefix_expression):
    """
    Evaluates a prefix expression directly, using a Deque.
    Operands must be integers and are initialy cast as ints.

    NOTE: Intermediate values may be floats (eg, 3/4 gives 0.75)
    so don't cast anything to int after the input phase.

    >>> evaluate_prefix('+ 2 4')
    6.0
    >>> evaluate_prefix('+ 2 * 4 3')
    14.0
    >>> evaluate_prefix('* + 2 * 1 2 8')
    32.0
    >>> evaluate_prefix('* - + 2 1 2 8')
    8.0
    >>> evaluate_prefix('+ / 8 - 6 2 4')
    6.0
    """

    # Split prefix string into tokens.
    tokens = re.findall(r'(\d+|\*|\+|\/|\-|\)|\(|\^)', prefix_expression)

    # add everything to a queue
    dq = Deque()
    for token in tokens:
        if token not in OP_PREC:
            # is a number so convert to a float
            token = float(token)
        dq.enqueue_rear(token)

    # etc
    # etc
    # etc
    # etc...
    # feel free to do this:)
    # and write some doctests to test that it works...




if __name__ == '__main__':
    #os.environ['TERM'] = 'linux' # Suppress ^[[?1034h

    # Uncomment the call to testmod to run the tests
    # Can enter an infinite loop if your Stack isn't implemented correctly
    # doctest.testmod()

    # Or you can test each thing separately
    doctest.run_docstring_examples(calculate, None)
    doctest.run_docstring_examples(evaluate_postfix, None)
    doctest.run_docstring_examples(infix_to_postfix, None)
    # doctest.run_docstring_examples(evaluate_infix, None)
    # doctest.run_docstring_examples(evaluate_prefix, None)

