#!/usr/bin/env python3

class Stack():
    """stack object class

    >>> from q1 import Stack
    >>> s = Stack()
    >>> s.push('a')
    >>> print('Peeked at', s.peek())
    Peeked at a
    >>> print('Popped', s.pop())
    Popped a
    >>> s.push('a')
    >>> s.push('b')
    >>> print('Peeked at', s.peek())
    Peeked at b
    >>> print('Length is', len(s))
    Length is 2
    >>> print('Popped', s.pop())
    Popped b
    >>> print('Length is', len(s))
    Length is 1
    >>> print('Popped', s.pop())
    Popped a
    >>> try:
    ...     s.pop()
    ... except IndexError as error:
    ...     print(error)
    ...
    Can't pop from empty stack!
    """
    def __init__(self):
        """initialise stack object"""
        self.items = []

    def push(self, item):
        """adds item to the top of the stack
            
        """
        self.items.append(item)

    def peek(self):
        """returns the latest item in the stack"""
        if len(self.items) > 0:
            return self.items[-1]

        return None

    def pop(self):
        """removes the latest item in the stack
           returns it as well"""
        if not self.items:
            raise IndexError("Can't pop from empty stack!")
        last_item = self.items.pop()
        return last_item

    def __len__(self):
        """returns the length of a stack"""
        return len(self.items)

    def __str__(self):
        """ Returns a nice string representation of the Stack """
        return "Bottom -> " + repr(self.items) + " <- Top"

    def __repr__(self):
        """ Returns a representation, simply the __str__
        This is useful for displaying the Stack in the shell
        """
        return str(self)
    
    

def main():
    import doctest
    doctest.testmod()

if __name__ == "__main__":
    main()
