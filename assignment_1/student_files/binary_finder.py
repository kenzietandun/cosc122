#!/usr/bin/env python3
""" Your docstring should go here
Along with your name and email address
Kenzie Tandun
kta79@uclive.ac.nz
"""

import classes

def simple_binary_plate_finder(stolen_list, sighted_list):
    """ Takes two lists of NumberPlates, returns a list and an integer.
    You can assume the stolen list will be in ascending order.
    You must assume that the sighted list is unsorted.

    The returned list contains stolen number plates that were sighted,
    in the same order as they appeared in the sighted list.
    The integer is the number of NumberPlate comparisons that
    were made.

    You can assume that each input list contains only unique plates,
    ie, neither list will contain more than one copy of any given plate.
    This fact will be very helpful in some special cases - you should
    think about when you can stop searching.    """
    result_list = []
    total_comparisons = 0
    total_found = 0

    # just return straight away if we don't need to 
    # search for anything
    # when either of the list is empty
    if not sighted_list or not stolen_list:
        return result_list, total_comparisons

    # iterate through all the plates in sighted_list
    for plate in sighted_list:
        # if we've found everything in the stolen list,
        # dont bother to continue iterating through sighted list 
        if total_found == len(stolen_list):
            break

        # uses search_plate to recursively search <plate> 
        # from list <stolen_list>
        # if it is found, <plate> will be returned again
        # 
        # the number of comparisons needed for the operation
        # is also returned
        found_plate, comparisons = search_plate(
                stolen_list, 
                plate, 
                end=len(stolen_list)-1)
        
        # if we found the plate add it to the <result_list> list
        if found_plate:
            total_found += 1
            result_list.append(found_plate)
        total_comparisons += comparisons

    return result_list, total_comparisons

def search_plate(item_pool, target_item, start=0, end=0, comparisons=0):
    """ searches plate from stolen list recursively with binary search """

    # REWRITE NUMBER 2
    # HOORAY
    
    # Base case
    # If there's only 1 item left, check if it's the plate
    # we are looking for
    if start == end:
        comparisons += 1
        if item_pool[start] == target_item:
            return target_item, comparisons
        return None, comparisons
    
    # If there are more than 1 items left, 
    # reduce it until it is only 1 item
    else:
        midpoint = (start + end) // 2
        pool_midpoint = item_pool[midpoint]
        # target item is larger than midpoint
        # call search_plate again but with upper half
        comparisons += 1
        if target_item > pool_midpoint:
            return search_plate(item_pool, target_item, 
                    start=midpoint+1, end=end, comparisons=comparisons)
        # target item is less or equal to midpoint
        # call search_plate again but with lower half
        else:
            return search_plate(item_pool, target_item,
                    start=start, end=midpoint, comparisons=comparisons)

def run_tests():
    """ For some simple tests while devloping your awesome answer code """
    from utilities import read_dataset
    filename = './test_data/5s-10-5-a.txt'
    stolen, sighted, matches = read_dataset(filename)
    found, comparisons = simple_binary_plate_finder(stolen, sighted)

if __name__ == '__main__':
    """ This won't run when your module is imported from.
    Use run_tests to try out some of your own simple tests.
    """
    run_tests()
