#!/usr/bin/env python3
""" Your docstring should go here
Author = Kenzie Tandun
Email = kta79@uclive.ac.nz
"""

import classes


def simple_linear_plate_finder(stolen_list, sighted_list):
    """
    Takes two lists of NumberPlates as input.
    Returns a list and an integer.

    The returned list contains stolen number plates that were sighted,
    in the same order as they appeared in the sighted list.
    The integer is the number of NumberPlate comparisons that
    were made.

    You cannot assume either list is sorted, ie, you should assume the
    lists are not sorted.

    You can assume that each input list contains only unique plates,
    ie, neither list will contain more than one copy of any given plate.
    This fact will be very helpful in some special cases - you should
    think about when you can stop searching.

    >>> simple_linear_plate_finder([], [])
    ([], 0)

    >>> simple_linear_plate_finder([123, 234, 345, 456], [345])
    ([345], 3)

    >>> simple_linear_plate_finder([123, 234], [123, 345])
    ([123], 3)

    >>> simple_linear_plate_finder([], [123, 345])
    ([], 0)

    >>> simple_linear_plate_finder([123, 234], [])
    ([], 0)
    """

    result_list = []
    # ---start student section---

    found = 0
    total_comparisons = 0
    for sighted_plate in sighted_list:
        if found == len(stolen_list):
            break
        for stolen_plate in stolen_list:
            total_comparisons += 1
            if stolen_plate == sighted_plate:
                found += 1
                result_list.append(stolen_plate)
                break

    # ===end student section===
    return result_list, total_comparisons


def run_tests():
    """ For some simple tests while devloping your awesome answer code """
    import doctest
    doctest.testmod()


if __name__ == '__main__':
    """ This won't run when your module is imported by the tests module.
    Use run_tests to try out some of your own simple tests.
    """
    run_tests()
