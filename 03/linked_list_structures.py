#!/usr/bin/env python3
"""Data structures implemented with linked lists.

Check out the comments/code at the end of this module
for how to run the provided doctests.

You should start by implementing the __str__ method as most
tests will print out Stacks or Queues. Hint, while loops will
be handy here.

"""

import doctest
import os


class Node:
    """A node for a linked list."""

    def __init__(self, initdata):
        self.data = initdata
        self.next_node = None


class Stack(object):
    """ Implements a Stack using a Linked List
    >>> s = Stack()
    >>> print(s)
    List for stack is: None
    >>> result = s.pop()
    Traceback (most recent call last):
    ...
    IndexError: Can't pop from empty stack.
    >>> s.push('a')
    >>> print(s)
    List for stack is: a -> None
    >>> len(s)
    1
    >>> print(s.head.data)
    a
    >>> print(s.head.next_node)
    None
    >>> s.pop()
    'a'
    >>> print(s)
    List for stack is: None
    >>> s.push('b')
    >>> print(s)
    List for stack is: b -> None
    >>> s.push('c')
    >>> print(s)
    List for stack is: c -> b -> None
    >>> len(s)
    2
    >>> s.peek()
    'c'
    >>> print(s)
    List for stack is: c -> b -> None
    >>> s.pop()
    'c'
    >>> print(s)
    List for stack is: b -> None
    >>> e = Stack()
    >>> e.peek()
    Traceback (most recent call last):
    ...
    IndexError: Can't peek at empty stack.
    """

    def __init__(self):
        self.head = None

    def push(self, item):
        """push a new item on to the stack"""
        # ---start student section---
        new_node = Node(item)
        new_node.next_node = self.head
        self.head = new_node
        # ===end student section===

    def pop(self):
        """pop an item off the top of the stack, and return it
        If stack is empty you should raise an IndexError as per
        the comment below."""
        # use the following line to raise error when stack is empty
        # raise IndexError("Can't pop from empty stack.")
        # ---start student section---
        if self.is_empty():
            raise IndexError("Can't pop from empty stack.")

        curr_node = self.head
        self.head = curr_node.next_node

        return curr_node.data
        # ===end student section===

    def peek(self):
        """pop an item on the top of the top of the stack, but don't remove it.
        If stack is empty you should raise an IndexError as per
        the comment below.
        """
        # use the following line to raise error when stack is empty
        # raise IndexError("Can't peek at empty stack.")
        # ---start student section---
        if self.is_empty():
            raise IndexError("Can't peek at empty stack.")

        return self.head.data
        # ===end student section===

    def is_empty(self):
        """ Returns True if stack is empty """
        return self.head is None

    def __len__(self):
        """ Returns the length --- calling len(s) will invoke this method """
        # ---start student section---
        length = 0
        curr_node = self.head
        while curr_node is not None:
            length += 1
            curr_node = curr_node.next_node

        return length
        # ===end student section===

    def __str__(self):
        """Returns a string representation of the list for the stack starting
        from the beginning of the list. Items are separated by ->
        and ending with -> None
        See doctests in class docstring
        """
        # ---start student section---
        template = "List for stack is: "
        curr_node = self.head
        while curr_node is not None:
            template += curr_node.data + " -> "
            curr_node = curr_node.next_node

        template += "None"
        return template
        # ===end student section===


class Queue(object):
    """ Implements a Queue using a Linked List"
    >>> q = Queue()
    >>> len(q)
    0
    >>> print(q)
    List for queue is: None
    >>> result = q.dequeue()
    Traceback (most recent call last):
    ...
    IndexError: Can't dequeue from empty queue.
    >>> q.enqueue('a')
    >>> q.dequeue()
    'a'
    >>> q.enqueue('a')
    >>> print(q)
    List for queue is: a -> None
    >>> len(q)
    1
    >>> q.enqueue('b')
    >>> print(q)
    List for queue is: a -> b -> None
    >>> q.enqueue('c')
    >>> print(q)
    List for queue is: a -> b -> c -> None
    >>> len(q)
    3
    >>> q.dequeue()
    'a'
    >>> print(q)
    List for queue is: b -> c -> None
    """

    def __init__(self):
        self.head = None

    def enqueue(self, item):
        """Add an item onto the tail of the queue.
        Note: The front of the queue is stored at the head of the list 
        so adding to the rear requires finding the end of the list
        """
        # ---start student section---
        # Create a new node, with next_item None
        new_node = Node(item)
        new_node.next_node = None

        if self.is_empty():
            self.head = new_node
            return

        # Traverse through current queue until we reach the end
        curr_node = self.head

        while curr_node.next_node is not None:
            curr_node = curr_node.next_node

        # Set the new node as the tail of the current queue
        curr_node.next_node = new_node
        # ===end student section===

    def dequeue(self):
        """Remove an item from the head of the queue and return it.
        If queue is empty you should raise an IndexError as per
        the comment below."""
        # use the following line to raise error when queue is empty
        # raise IndexError("Can't dequeue from empty queue.")
        # ---start student section---
        if self.is_empty():
            raise IndexError("Can't dequeue from empty queue.")

        curr_node = self.head
        self.head = curr_node.next_node

        return curr_node.data
        # ===end student section===

    def is_empty(self):
        """ returns True if the queue is empty """
        # ---start student section---
        return self.head is None
        # ===end student section===

    def __len__(self):
        """ Returns the length --- calling len(q) will invoke this method """
        # ---start student section---
        length = 0
        curr_node = self.head
        while curr_node is not None:
            length += 1
            curr_node = curr_node.next_node

        return length
        # ===end student section===

    def __str__(self):
        """Returns a string representation of the list for the queue starting
        from the beginning of the list. Items are separated by ->
        and ending with -> None
        See doctests in class docstring
        """
        # ---start student section---
        template = "List for queue is: "

        curr_node = self.head
        while curr_node is not None:
            template += curr_node.data + " -> "
            curr_node = curr_node.next_node

        template += "None"

        return template
        # ===end student section===


if __name__ == '__main__':
    os.environ['TERM'] = 'linux'  # Suppress ^[[?1034h
    
    
    # Ucomment the relevant doc test run line below
    # Be careful your can get infinite loops if done wrong... so try with Debug
    #doctest.run_docstring_examples(Stack, None)
    #doctest.run_docstring_examples(Queue, None)
    
    
    # Uncomment the testmod() line to run all the tests
    # Can enter an infinite loop if your Stack isn't implemented correctly
    doctest.testmod()
