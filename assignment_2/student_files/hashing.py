#!/usr/bin/env python3

""" Module containing classes and functions for students to complete.
Read the notes with each question in the submission quiz for more
details on what to submit.
The submission quiz will open around a week before the due date
so you can check your code vs. the quiz server's pylint etc.
You should be using your own tests to help you debug your code
and the provided tests.py as final checks that your code works
with bigger data sets. Passing tests.py doesn't necessarily
mean that your code works so make sure you don't rely solely
on those tests...

Author: Kenzie Tandun
Date: 24 Sep 2018

"""

import doctest
from classes2 import NumberPlate
from stats import StatCounter




class ListTable:
    """ This is not really a hashtable.
        It stores (plate, value) tuples in a list.
        It uses a simple linear scan through the whole list
        to find a plate.
        When setting a plate's value it scans through the whole list
        to see if the plate is there first. If it is there it updates
        the plate's value, otherwise it appends a (plate, value) tuple
        to the end of the list.
    """

    def __init__(self, n_slots=0):
        """ Note n_slots is ignored it's there for compatability
        Each (plate, value) record/pair is stored as a tuple.
        Any new records should be simply appended to the data_list.
        The data_list isn't sorted.
        """
        self.data_list = []
        self.n_items = 0
        self.n_slots = 0   # to keep pylint happy :)
        # NOTE:
        # The following two variables must be updated y your code
        self.n_plate_comparisons = 0   # update whenever number plates are compared
        # update whenever hash(number_plate) is called
        self.n_plate_hashes = 0
        # special note for this class no hashes are ever calculated...
        # the variable is left in for compatability with the testing code.

    def __setitem__(self, plate, value):
        """ Called when mylisttable[plate] = value assignment is used.
        Will update the value for the plate if the plate already in the list
        or will append a new record with (plate, flag) to the end of the list.
        >>> table = ListTable()
        >>> plate1 = NumberPlate('ABC123')
        >>> table[plate1] = 'Banana 3'
        >>> plate2 = NumberPlate('BOB456')
        >>> table[plate2] = 'Banana 2'
        >>> plate3 = NumberPlate('JOE234')
        >>> table[plate3] = 'Orange 1'
        >>> print(table)
        List Table:
        0-> ('ABC123', 'Banana 3')
        1-> ('BOB456', 'Banana 2')
        2-> ('JOE234', 'Orange 1')
        <BLANKLINE>
        >>> # update plate2 to have the value 'Banana 1'
        >>> table[plate2] = 'Banana 1'
        >>> print(table)
        List Table:
        0-> ('ABC123', 'Banana 3')
        1-> ('BOB456', 'Banana 1')
        2-> ('JOE234', 'Orange 1')
        <BLANKLINE>
        """
        record = (plate, value)
        i = 0
        updated = False
        while i < len(self.data_list) and not updated:
            current_plate, value = self.data_list[i]
            self.n_plate_comparisons += 1
            if current_plate == plate:
                self.data_list[i] = record
                updated = True
            i += 1
        if not updated:
            self.data_list.append(record)
            self.n_items += 1

    def __contains__(self, plate):
        """ Called when doing plate in table expressions.
        Will return True if the plate is in the table, otherwise returns False
        >>> table = ListTable()
        >>> plate1 = NumberPlate('ABC123')
        >>> table[plate1] = 'Banana 3'
        >>> plate2 = NumberPlate('BOB456')
        >>> table[plate2] = 'Banana 2'
        >>> plate3 = NumberPlate('JOE234')
        >>> table[plate3] = 'Orange 1'
        >>> print(table)
        List Table:
        0-> ('ABC123', 'Banana 3')
        1-> ('BOB456', 'Banana 2')
        2-> ('JOE234', 'Orange 1')
        <BLANKLINE>
        >>> print(plate1 in table)
        True
        >>> print(plate2 in table)
        True
        >>> print(plate3 in table)
        True
        >>> print(NumberPlate('ZZZ999') in table)
        False
        """
        found = False
        i = 0
        while i < len(self.data_list) and not found:
            current_plate, _ = self.data_list[i]
            self.n_plate_comparisons += 1
            if current_plate == plate:
                found = True
            else:
                i += 1
        return found

    def __getitem__(self, plate):
        """ Returns the value for atable[plate], see examples below.
        >>> table = ListTable()
        >>> plate1 = NumberPlate('ABC123')
        >>> table[plate1] = 'Banana 1'
        >>> plate2 = NumberPlate('BOB456')
        >>> table[plate2] = 'Banana 2'
        >>> plate3 = NumberPlate('JOE234')
        >>> table[plate3] = 'Orange 1'
        >>> print(table)
        List Table:
        0-> ('ABC123', 'Banana 1')
        1-> ('BOB456', 'Banana 2')
        2-> ('JOE234', 'Orange 1')
        <BLANKLINE>
        >>> print(table[plate1])
        Banana 1
        >>> print(table[plate2])
        Banana 2
        >>> print(table[plate3])
        Orange 1
        >>> unknown = NumberPlate('ZZZ999')
        >>> print(table[unknown])
        None
        """
        found = False
        i = 0
        while i < len(self.data_list) and not found:
            current_plate, current_value = self.data_list[i]
            self.n_plate_comparisons += 1
            if current_plate == plate:
                found = True
            else:
                i += 1
        if not found:
            current_value = None
        return current_value

    def items(self):
        """ Generator that yields all the (plate, value) tuples in the table.
            Note the value could be a flag or a list of time stamps, or any other
            thing that you would like to store in the table...
        """
        for record in self.data_list:
            if record:
                yield record

    def keys(self):
        """ Generator that yields all the plates/keys in the table """
        for record in self.data_list:
            if record:
                plate, _ = record
                yield plate

    def __len__(self):
        """ Returns the number of items stored in the hash table """
        return self.n_items

    def __str__(self):
        result = 'List Table:\n'
        for i, value in enumerate(self.data_list):
            result += '{}-> {}\n'.format(i, value)
        return result

    def condensed_str(self):
        """ The list has no gaps so it's already condensed :)
        The only thing different here is the title line...
        """
        result = 'List Table (condensed):\n'
        for i, value in enumerate(self.data_list):
            result += '{}-> {}\n'.format(i, value)
        return result


class LinearHashTable:
    """ Your interesting class docstring goes here """

    def __init__(self, n_slots):
        if n_slots <= 0:
            raise ValueError('Come on! A hashtable needs at least 1 slot!')
        self.n_slots = n_slots
        self.table_list = [None] * n_slots  # make empty hash table
        self.n_items = 0
        # NOTE:
        # The following two variables must be updated by your code
        self.n_plate_comparisons = 0   # update whenever number plates are compared
        # update whenever hash(number_plate) is called
        self.n_plate_hashes = 0

    def __setitem__(self, plate, value):
        """
        Sets the value for the given plate. Basically stores the (plate, value) tuple
        at the appropriate index in the table_list.

        For example, the following will put a (plate, flag) tuple in the appropriate place
        in the table_list:
        my_table[plate] = flag

        Should raise IndexError('Hashtable is full!') if table is full.
        Note: Whilst you cannot add a new plate to a full table, it is still possible
        to update the value for a plate that is already in the table.
        """
        # ---start student section---
        if self.n_items >= self.n_slots:
            IndexError('Hashtable is full!') 

        self.n_plate_hashes += 1
        plate_hash = hash(plate) % self.n_slots
        while self.table_list[plate_hash]:
            found_plate, _ = self.table_list[plate_hash]
            self.n_plate_comparisons += 1
            if found_plate != plate:
                plate_hash = (plate_hash + 1) % self.n_slots
            else:
                break

        self.table_list[plate_hash] = (plate, value)
        self.n_items += 1
        # ===end student section===

    def __contains__(self, plate):
        """ Returns True if the plate is in the table, otherwise False. """
        # ---start student section---
        self.n_plate_hashes += 1
        plate_hash = hash(plate) % self.n_slots
        
        counter = 0
        while self.table_list[plate_hash] and counter <= self.n_slots:
            counter += 1
            found_plate, _ = self.table_list[plate_hash]
            plate_hash = (plate_hash + 1) % self.n_slots
            self.n_plate_comparisons += 1
            if found_plate == plate:
                return True
        return False
        # ===end student section===

    def __getitem__(self, plate):
        """ Returns the value associated with the given plate
            or None if the plate isn't in the table.
            Note the value could be a flag or a list of time stamps, or any other
            thing that you would like to store in the table...
            
            For example using the following will invoke this method:
            flag = my_linear_hashtable[plate]
        """
        # ---start student section---
        self.n_plate_hashes += 1
        plate_hash = hash(plate) % self.n_slots

        counter = 0
        while self.table_list[plate_hash] and counter <= self.n_slots:
            counter += 1
            found_plate, value = self.table_list[plate_hash]
            self.n_plate_comparisons += 1
            if found_plate == plate:
                return value
            plate_hash = (plate_hash + 1) % self.n_slots
        return None
        # ===end student section===

    def items(self):
        """ Generator that yields all the (plate, value) tuples in the table.
        Note the value could be a flag or a list of time stamps, or any other
        thing that you would like to store in the table...
        Usage example:
        for key, value in my_table.items():
            print(key, value)
        """
        for record in self.table_list:
            if record:
                yield record

    def keys(self):
        """ Generator that yields all the plates/keys in the table """
        for record in self.table_list:
            if record:
                plate, _ = record
                yield plate

    def __len__(self):
        """ Returns the number of items stored in the table """
        return self.n_items

    def __str__(self):
        """ Returns a nice version of the table with one slot per line """
        result = 'Linear Hash Table:\n'
        for i, slot_record in enumerate(self.table_list):
            result += '{}-> {}\n'.format(i, slot_record)
        return result

    def condensed_str(self):
        """ Similar to the __str__ method by only includes
            slots that are not empty
        """
        result = 'Linear Hash Table (condensed):\n'
        for i, slot_record in enumerate(self.table_list):
            if slot_record:
                result += '{}-> {}\n'.format(i, slot_record)
        return result


class ChainingHashTable:
    """ Your interesting class docstring goes here """

    def __init__(self, n_slots):
        self.n_items = 0  # starts out empty
        if n_slots <= 0:
            raise ValueError('Come on! A hashtable needs at least 1 slot!')
        self.n_slots = n_slots  # must provide the number of slots to be used
        # Note: each slot in the table_list contains the list/chain of items
        # that hash to that index. A simple Python list is used for this.
        self.table_list = [[] for _ in range(n_slots)]
        # The following two variables must be updated by your code
        self.n_plate_comparisons = 0   # update whenever number plates are compared
        # update whenever hash(number_plate) is called
        self.n_plate_hashes = 0

    def __setitem__(self, plate, value):
        """
        Sets the value for the given plate. Basically stores the (plate, value) tuple
        in the list at the appropriate index in the table_list.
        Note the value could be a flag or a list of time stamps, or any other
        thing that you would like to store in the table...
        
        Each slot in the table_list will contain a list of (plate, value) pairs.
        Using this table to store flags would mean the value for each plate would be a flag string.
        Using this table to store timestamps will mean the value for each plate will be a list
        of timestamps.
        For example, the following will put a (plate, timestamps) tuple in the appropriate place
        in the table_list:
        my_table[plate] = timestamps

        """
        # ---start student section---
        self.n_plate_hashes += 1
        plate_hash = hash(plate) % self.n_slots
        for i in range(len(self.table_list[plate_hash])):
            stored_plate, _ = self.table_list[plate_hash][i]
            self.n_plate_comparisons += 1
            if stored_plate == plate:
                self.table_list[plate_hash][i] = (plate, value)
                break
        else:
            self.table_list[plate_hash].append((plate, value))
                
        self.n_items += 1
        # ===end student section===

    def __contains__(self, plate):
        """ Returns True if the plate is in the table, otherwise False. """
        # ---start student section---
        self.n_plate_hashes += 1
        plate_hash = hash(plate) % self.n_slots
        for stored_plate, _ in self.table_list[plate_hash]:
            if stored_plate == plate:
                return True
        # ===end student section===
        return False

    def __getitem__(self, plate):
        """ Returns the value associated with the given plate
            or None if the plate isn't in the table.
            For example using the following will invoke this method:
            flag = my_linear_hashtable[plate]
        """
        # ---start student section---
        self.n_plate_hashes += 1
        plate_hash = hash(plate) % self.n_slots
        for stored_plate, stored_value in self.table_list[plate_hash]:
            self.n_plate_comparisons += 1
            if stored_plate == plate:
                return stored_value
        # ===end student section===
        return None

    def items(self):
        """ Generator that yields all the (plate, value) tuples in the table.
        Note the value could be a flag or a list of time stamps, or any other
        thing that you would like to store in the table...
        Usage example:
        for key, value in my_table.items():
            print(key, value)
        """
        for slot_list in self.table_list:
            if slot_list:
                for record in slot_list:
                    yield record

    def keys(self):
        """ Generator that yields all the plates/keys in the table """
        for slot_list in self.table_list:
            if slot_list:
                for record in slot_list:
                    plate, _ = record
                    yield plate

    def __len__(self):
        """ Returns the number of items stored in the hash table"""
        return self.n_items

    def __str__(self):
        """ Returns a nice version of the table with one slot per line """
        result = 'Chaining Hash Table:\n'
        for i, slot_list in enumerate(self.table_list):
            result += '{}-> {}\n'.format(i, slot_list)
        return result

    def condensed_str(self):
        """ Similar to the __str__ method by only includes slots that are not empty """
        result = 'Chaining Hash Table (condensed):\n'
        for i, slot_list in enumerate(self.table_list):
            if slot_list:
                result += '{}-> {}\n'.format(i, slot_list)
        return result


def make_db_hash_table(database_list, n_slots, table_class=LinearHashTable):
    """ Makes a hash table of the given class from the (plate, flag) tuples
    in the database_list.
    The key used for storing the values should be the number_plate.
    The items from the database list should be added to the table in
    the order that they are given in the database list.
    For example:
    >>> database_list = [(NumberPlate('AAA321'), 'Alien')]
    >>> database_list.append((NumberPlate('BAA900'), ''))
    >>> table = make_db_hash_table(database_list, 5)
    >>> print(table)
    Linear Hash Table:
    0-> None
    1-> None
    2-> None
    3-> ('AAA321', 'Alien')
    4-> ('BAA900', '')
    <BLANKLINE>
    """
    db_table = table_class(n_slots)
    # ---start student section---
    for plate, value in database_list:
        db_table[plate] = value
    # ===end student section===
    return db_table


def process_camera_stream(database_list, sightings, db_table_size, flagged_table_size):
    """
    Makes a LinearHashTable with db_table_size slots and adds all the plates to
    the database with their flags as (plate, flag) tuples. Even plates with
    empty flag strings should be added to the hash database table.

    Then makes a result table that is a ChaingingHashTable with flagged_table_size slots.
    The value stored for each plate with be a tuple containing (plate, list_of_timestamps).
    Then the function scans through the sightings and records the timestamps for flagged plates
    in the results table. NOTE: You only need to record the timestamps for number plates
    that have a flag in the database (ie, their flag is not just '').

    If a plate is already in the result table then the timestamp should be appended to the list
    of times for that plate.
    If a plate isn't aleady in the result table then it should be appended to the
    list of (plate, timestamps) tuples held in the list/chain in the slot that the plate
    hashes to.

    Check out some of the expected_results_table file files to get a feel for how the
    results table should look.
    """
    # make the database table using using your make_db_hash_table function
    db_table = make_db_hash_table(
        database_list, db_table_size, table_class=LinearHashTable)

    # make an empty chaning hash table to put results in
    results_table = ChainingHashTable(flagged_table_size)
    # make the resultss table
    # ---start student section---
    for plate, timestamp in sightings:
        if plate in db_table and db_table[plate]:
            if plate not in results_table.keys():
                results_table[plate] = []
            results_table[plate].append(timestamp)
    # ===end student section===
    # return a tuple containing the db_talbe and the resultss table
    return db_table, results_table


def trial_hash_table(plate_str_list, table):
    """ docstring for trial_hash_table """
    for plate_str in ['ABC123', 'DUF721', 'EGG001', 'CSC001', 'ACC101', 'BIG404']:
        temp = NumberPlate(plate_str)
        table[temp] = 'Stolen' + str(temp)
        assert table[temp] == 'Stolen' + str(temp)
    print(table)
    print()
    print(table.condensed_str())


def run_simple_tests():
    """ A nice place for your testing code """
    pass


def example_linearhash_stuff():
    """ This example uses a linear hash table to store
    the flags for some number plates, ie, a small
    database of flags for plates.
    """
    plate = NumberPlate('ABC231')
    flag = 'Overdue fines'
    table = LinearHashTable(11)
    table[plate] = flag        # translates to table.__set_item__(flag)
    print(plate in table)      # translates to table.__contains__(flag)
    print(table[plate])        # translates to table.__get_item__(plate)
    print(table)
    table[plate] = 'new_flag'  # translates to table.__set_item__(plate)
    # add a new plate and flag to the table
    plate2 = NumberPlate('BBB222')
    table[plate2] = 'BBB222-flag'
    print(table)
    print('\n' * 3)


def example_chaining_stuff():
    """ Example use of the chaingin hash table.
    This example uses a chaining hash table to store
    the flags for some number plates, ie, a small
    database of flags for plates.
    """
    plate1 = NumberPlate('BAA754')
    plate2 = NumberPlate('MOO123')
    plate3 = NumberPlate('WOF833')
    plate4 = NumberPlate('EEK003')

    table = ChainingHashTable(5)
    table[plate1] = 'Sheep'
    table[plate2] = 'Cow'
    table[plate3] = 'Dog'
    table[plate4] = 'Mouse'
    print(table)

    print('plate1 in table:', plate1 in table)
    print('plate2 in table:', plate2 in table)
    print('plate3 in table:', plate3 in table)
    print('plate4 in table:', plate4 in table)
    print()
    print('Value for plate1:', table[plate1])
    print('Value for plate2:', table[plate2])
    print('Value for plate3:', table[plate3])
    print('Value for plate4:', table[plate4])
    print()
    table[plate1] = 'new_flag'  # update the value for plate 1
    plate5 = NumberPlate('BBB222')  # add in a new plate
    table[plate5] = 'BBB222-flag'
    print(table)
    print('plate5 (BBB222) in table:', plate5 in table)


if __name__ == '__main__':
    plate = NumberPlate("ABC231")
    flag = "Overdue fines"
    table = ChainingHashTable(11)
    table[plate] = flag
    print(plate in table)
    print(table[plate])
    table[plate] = "new_flag"
    plate2 = NumberPlate("BBB222")
    table[plate2] = "BBB222-flag"
    table[NumberPlate("ANC121")] = "Test1"
    table[NumberPlate("ANC122")] = "Test2"
    table[NumberPlate("ANC123")] = "Test3"
    table[NumberPlate("ANC124")] = "Test4"
    table[NumberPlate("ANC125")] = "Test5"
    table[NumberPlate("ANC126")] = "Test6"
    print(NumberPlate("ANC126") in table)
    print(table[NumberPlate("ANC121")])
    print(table)
    # uncomment the next line to run the ListTable doctests
    doctest.testmod()

    # various examples for you to run and expand upon
    # run_simple_tests()
    # example_linearhash_stuff()
    # example_chaining_stuff()
